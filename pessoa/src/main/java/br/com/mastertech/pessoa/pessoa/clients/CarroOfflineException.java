package br.com.mastertech.pessoa.pessoa.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "O sistema de carro se encontra offline")
public class CarroOfflineException extends RuntimeException {
}
