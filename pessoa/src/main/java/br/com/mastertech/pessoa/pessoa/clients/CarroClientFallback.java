package br.com.mastertech.pessoa.pessoa.clients;

import br.com.mastertech.pessoa.pessoa.model.Carro;

public class CarroClientFallback implements CarroClient {

    @Override
    public Carro getCarroByPlaca(String placa) {
//        Carro carro = new Carro();
//
//        carro.setModelo("Galinha pintadinha de rodas");
//        carro.setPlaca("PIU-PIU3");
//
//        return carro;
        throw new CarroOfflineException();
    }
}
