package br.com.mastertech.pessoa.pessoa.clients;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CarroClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new InvalidCarroException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }

}
