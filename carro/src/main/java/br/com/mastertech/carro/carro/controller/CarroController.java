package br.com.mastertech.carro.carro.controller;

import br.com.mastertech.carro.carro.model.Carro;
import br.com.mastertech.carro.carro.service.CarroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class CarroController {

    @Autowired
    private CarroService carroService;

    @PostMapping
    public Carro create(@RequestBody Carro carro) {
        return carroService.create(carro);
    }

    @GetMapping("/{placa}")
    public Carro getByPlaca(@PathVariable String placa) {
        System.out.println("Recebi uma requisição! " + System.currentTimeMillis());
        return carroService.getByPlaca(placa);
    }


}
