package br.com.mastertech.carro.carro.exceptions;

import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Carro nao encontrado.")
public class CarroNotFoundException extends RuntimeException {
}
